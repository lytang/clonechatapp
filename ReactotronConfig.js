import Reactotron from 'reactotron-react-native'
import { reactotronRedux as reduxPlugin } from 'reactotron-redux'

console.disableYellowBox = true

Reactotron.configure({name: 'ChatApp'})

Reactotron.useReactNative({
  asyncStorage: { ignore: ['secret'] }
})

Reactotron.use(reduxPlugin())

console.tron = Reactotron