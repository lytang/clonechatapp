import React, { Component } from "react";
import configureStore from './src/store';
import { Provider, connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './src/actions';
// const Realm = require('realm');
import Login from './src/pages/Login';
import Listing from './src/pages/Listing';
import ChatRoom from './src/pages/ChatRoom';
import DirectChatRoom from './src/pages/DirectChatRoom';
import Detail from './src/pages/Detail';
import OneSignal from 'react-native-onesignal';
import {
  StackNavigator,
  NavigationActions,
  StackActions
} from 'react-navigation';
import {apiUrl} from "./src/constants/apiUrl";
import { PersistGate } from 'redux-persist/es/integration/react';
import Reactotron from 'reactotron-react-native';
// import './ReactotronConfig'
import BadgeAndroid from 'react-native-android-badge';
const { persistor, store } = configureStore()

const mapActionsAndStore = (Page)=>{
  const stateAndProps = connect(
    state => state,
    dispatch => bindActionCreators(actions, dispatch)
  )
  return stateAndProps(Page);
};

const AppNavigator = StackNavigator({
  Login: { screen: mapActionsAndStore(Login) },
  Listing: { screen: mapActionsAndStore(Listing) },
  ChatRoom: { screen: mapActionsAndStore(ChatRoom) },
  DirectChatRoom: {screen: mapActionsAndStore(DirectChatRoom)},
  Detail: { screen: mapActionsAndStore(Detail) },
});


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      userId: null,
      userToken: null,
      newNotification:false,
    };
  }
  componentWillMount(){
    // BadgeAndroid.setBadge(10)
    OneSignal.init("c53de855-8950-4f67-b2e3-d0e7baf38e41");
    permissions = {
      alert: true,
      badge: true,
      sound: true
    };
    OneSignal.requestPermissions(permissions);
    OneSignal.inFocusDisplaying(2)
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    OneSignal.configure()
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.configure()
  }

  componentDidMount(){
    // console.log(this.refs)
    // permissions = {
    //   alert: true,
    //   badge: true,
    //   sound: true
    // };
    // OneSignal.requestPermissions(permissions);
    // OneSignal.inFocusDisplaying(2)
    // OneSignal.addEventListener('ids', this.onIds.bind(this));
    // OneSignal.addEventListener('opened', this.onOpened.bind(this));
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onIds(device) {
    this.setState({userId: device.userId, newNotification:true}, ()=>{
      this.updateDeviceToken()
    })
  }

  changeNotificationStatus(){
    this.setState({newNotification:false})
  }

  getUserId(){
    return this.state.userId;
  }

  onOpened(openResult){
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
    const { additionalData } = openResult.notification.payload;
    const chatRoom = additionalData;
    this.goToChatRoom(chatRoom);
  }

  goToChatRoom(chatRoom){
    const { navigatior }  = this.refs;
    if(!navigatior){
      setTimeout(()=>this.goToChatRoom(chatRoom), 1000);
      return;
    }
    const resetAction = StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({ routeName: 'Listing' }),
        NavigationActions.navigate({
          routeName: 'ChatRoom',
          params: {
            chatRoom: chatRoom
          }
        })
      ]
    })
    navigatior.dispatch(resetAction)
  }

  setUserToken(token){
    // alert(token)
    this.setState({
      userToken: token
    }, ()=>{
      this.updateDeviceToken()
    })
  }

  updateDeviceToken(){
    // console.log('updateDeviceToken')
    const { userToken, userId } = this.state;
    if(userToken && userId){
      // fetch('http://bchetthor.thadatra.com/api/device-update', {
      fetch(`${apiUrl}/api/device-update`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          apitoken: userToken,
          deviceId: userId,
        },
      }).then((data)=>{
        // alert(JSON.stringify(data))
        console.log('device-update',data)
      })
    }
  }

  render() {
    // alert('hello')
    console.log(this.props)
    // console.tron.log('hello')
    // console.tron.log(this.props)
    return (
      <Provider store={store}>
        <PersistGate
          persistor={persistor}>
          <AppNavigator ref='navigatior' 
          screenProps={{
            userId: this.getUserId.bind(this),
            setUserToken: this.setUserToken.bind(this),
            newNotification: this.state.newNotification,
            changeNotificationStatus:  this.changeNotificationStatus.bind(this)
          }}
          // changeNotificationStatus= {()=> this.changeNotificationStatus.bind(this)}
           />
            
        </PersistGate>
      </Provider>
    )
  }
}

export default App;
