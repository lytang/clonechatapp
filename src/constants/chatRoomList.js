const ADD_CHATROOMS='ADD_CHATROOMS';
const GET_ALL_CHATROOMS = 'GET_ALL_CHATROOMS';
const GET_ALL_CHATROOMS_REQUEST = 'GET_ALL_CHATROOMS_REQUEST';
const GET_ALL_CHATROOMS_FAILURE = 'GET_ALL_CHATROOMS_FAILURE ';
const GET_ALL_CHATROOMS_SUCCESS = 'GET_ALL_CHATROOMS_SUCCESS';
module.exports =  {
  ADD_CHATROOMS,
  GET_ALL_CHATROOMS,
  GET_ALL_CHATROOMS_REQUEST,
	GET_ALL_CHATROOMS_FAILURE,
	GET_ALL_CHATROOMS_SUCCESS
}
