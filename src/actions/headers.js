import { UPDATE_HEADERS, RESET_HEADERS } from '../constants';

const updateHeaders = (value)=>({
  type: UPDATE_HEADERS,
  value
})

const resetHeaders = ()=>({
  type: RESET_HEADERS
})

export default {
  updateHeaders,
  resetHeaders
}
