import { GET_ALL_MESSAGES_SUCCESS, GET_ALL_MESSAGES_REQUEST,GET_ALL_MESSAGES_FAILURE } from '../constants';
import {apiUrl} from '../constants/apiUrl';

export function getAllMessages (headers, groupId, memberId, numOfMessages){
	// alert('action')
return (dispatch) => {
		// dispatch(getAllChatRoomsRequest())
		// alert('action')
    const url = `${apiUrl}/api/messages-list`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        group_id:groupId,
        count: numOfMessages,
        // member_id:memberId
      })
    })
    .then((response)=>{
      // this.props.getAllChatRooms(response)
      // alert(JSON.stringify('test'))
      return response.json()
    })
    .then((messages)=>{
      console.log(messages)
      // alert(JSON.stringify(chatRooms))
      // this.setState({ loading: false })
      // this.setState({ chatRooms: chatRooms });
      // getAllChatRooms(chatRooms)
      // return (dispatch)=>{dispatch(getAllChatRoomsSuccess(chatRooms))}
      dispatch(getAllMessagesSuccess(messages))
    }).catch((error)=>{
    	alert(JSON.stringify(error))
    });
	}


}
const getAllMessagesRequest = ()=>{
	return {
		type: GET_ALL_MESSAGES_REQUEST
	}
}
const getAllMessagesSuccess= (messages)=>{
	return {
		type: GET_ALL_MESSAGES_SUCCESS,
		data:messages
	}
}
// export default {
//   addChatRooms,
//   getAllChatRooms
// }
