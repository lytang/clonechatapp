import {  GET_ALL_USERINFO_SUCCESS, GET_ALL_USERINFO_REQUEST,GET_ALL_USERINFO_FAILURE } from '../constants';
import {apiUrl} from '../constants/apiUrl';
import * as UserModel from '../models/user'
export const addChatRooms = (chatRooms)=>({
  type: ADD_CHATROOMS,
  chatRooms
})

export function getAllUserInfo (headers){
return (dispatch) => {
		// dispatch(getAllChatRoomsRequest())
		// alert('action')
    const url = `${apiUrl}/api/user-info`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response)=>{
      // this.props.getAllChatRooms(response)
      // alert(JSON.stringify('test'))
      return response.json()
    })
    .then((userInfo)=>{
      console.log(userInfo)
      dispatch(getAllUserInfoSuccess(userInfo))
      // dispatch(addUser(userInfo))
      // UserModel.getData('add',userInfo)
    }).catch((error)=>{
    	alert(JSON.stringify(error))
    });
	}


}

export function getUserInfoOffline(){
  alert(JSON.stringify(UserModel.getData('list')))
  // .then((data)=>{
  //   alert(JSON.stringify(data))
  // })
}
const getAllUserInfoRequest = ()=>{
	return {
		type: GET_ALL_USERINFO_REQUEST
	}
}
const getAllUserInfoSuccess= (userInfo)=>{
	return {
		type: GET_ALL_USERINFO_SUCCESS,
		data:userInfo
	}
}
// export default {
//   addChatRooms,
//   getAllChatRooms
// }
