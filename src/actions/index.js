import Login from './login';
import ChatRoomList from './chatRoomList';
import Headers from './headers';
import User from './user';

export default {
  ...Login,
  ...Headers,
  ...ChatRoomList,
  ...User,
}
