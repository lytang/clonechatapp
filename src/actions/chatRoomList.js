import { ADD_CHATROOMS, GET_ALL_CHATROOMS, GET_ALL_CHATROOMS_SUCCESS,
	GET_ALL_CHATROOMS_REQUEST,GET_ALL_CHATROOMS_FAILURE, GET_ALL_DIRECTCHATROOMS_SUCCESS, GET_ALL_DIRECTCHATROOMS_FAILURE,
	GET_ALL_DIRECTCHATROOMS_REQUEST } from '../constants';
import {apiUrl} from '../constants/apiUrl';
export const addChatRooms = (chatRooms)=>({
  type: ADD_CHATROOMS,
  chatRooms
})

export function getAllChatRooms (headers){
	// alert('action')
return (dispatch) => {
		// dispatch(getAllChatRoomsRequest())
		// alert('action')
    const url = `${apiUrl}/api/groups`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response)=>{
      // this.props.getAllChatRooms(response)
      // alert(JSON.stringify('test'))
      return response.json()
    })
    .then((chatRooms)=>{
      console.log(chatRooms)
      // alert(JSON.stringify(chatRooms))
      // this.setState({ loading: false })
      // this.setState({ chatRooms: chatRooms });
      // getAllChatRooms(chatRooms)
      // return (dispatch)=>{dispatch(getAllChatRoomsSuccess(chatRooms))}
      dispatch(getAllChatRoomsSuccess(chatRooms))
    }).catch((error)=>{
    	alert(JSON.stringify(error))
    });
	}


}

export function getAllDirectChatRooms (headers, userId){
	// alert('action')
return (dispatch) => {
		// dispatch(getAllChatRoomsRequest())
		// alert('action')
    const url = `${apiUrl}/api/direct-message-list`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
      	user_id: userId
      })
    })
    .then((response)=>{
      // this.props.getAllChatRooms(response)
      // alert(JSON.stringify('test'))
      return response.json()
    })
    .then((directChatRooms)=>{
      console.log(directChatRooms)
      // alert(JSON.stringify(chatRooms))
      // this.setState({ loading: false })
      // this.setState({ chatRooms: chatRooms });
      // getAllChatRooms(chatRooms)
      // return (dispatch)=>{dispatch(getAllChatRoomsSuccess(chatRooms))}
      dispatch(getAllDirectChatRoomsSuccess(directChatRooms))
    }).catch((error)=>{
    	// alert(JSON.stringify(error))
    	console.log(error)
    });
	}


}
const getAllDirectChatRoomsSuccess = (directChatRooms)=>{
	return {
		type: GET_ALL_DIRECTCHATROOMS_SUCCESS,
		data: directChatRooms
	}
}

const getAllChatRoomsRequest = ()=>{
	return {
		type: GET_ALL_CHATROOMS_REQUEST
	}
}
const getAllChatRoomsSuccess= (chatRooms)=>{
	return {
		type: GET_ALL_CHATROOMS_SUCCESS,
		data:chatRooms
	}
}
// export default {
//   addChatRooms,
//   getAllChatRooms
// }
