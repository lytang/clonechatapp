import { UPDATE_PHONE_NUMBER } from '../constants';

const updatePhoneNumber = (phoneNumber)=>({
  type: UPDATE_PHONE_NUMBER,
  phoneNumber
})


export default {
  updatePhoneNumber
}
