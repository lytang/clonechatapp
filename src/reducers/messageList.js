import {
  GET_ALL_MESSAGES_SUCCESS,
  GET_ALL_MESSAGES_REQUEST,
  GET_ALL_MESSAGES_FAILURE,
} from '../constants/index';
const initState = {
 messages:[],
 isFetching:false,
 error:''
}
const messageList = (state = initState, action) => {

  switch(action.type){
    case GET_ALL_MESSAGES_SUCCESS:
    	return { ...state,messages: action.data, isFetching:false, error:''}
    case GET_ALL_MESSAGES_REQUEST:
    	return { ...state, isFetching:true, error:''}
    case GET_ALL_MESSAGES_FAILURE:
    	return { ...state, isFetching:false, error:action.error}
    default:
      return state;
  }
}


export default messageList