import { UPDATE_PHONE_NUMBER } from '../constants';


export const login = (state = {}, action) => {
  switch(action.type){
    case UPDATE_PHONE_NUMBER:
      return updatePhoneNumber(state, action);
    default:
      return state;
  }
}

const updatePhoneNumber = (state, action)=>{
  return {...state, phoneNumber: action.phoneNumber}
}
