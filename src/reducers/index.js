import { combineReducers } from 'redux';
import { login } from './login';
import { headers } from './headers';
import chatRoomList  from './chatRoomList';
import messageList from './messageList';
import userInfoList from './user';

import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage'

const config = {
  key: 'root',
  storage,
  whitelist: ['headers'],
}

export default persistCombineReducers( config, {
  login,
  headers,
  chatRoomList,
  messageList,
  userInfoList
})
