import {
  UPDATE_HEADERS,
  RESET_HEADERS
} from '../constants/headers';


export const headers = (state = {}, action) => {
  switch(action.type){
    case UPDATE_HEADERS:
      return updateHeaders(state, action);
    case RESET_HEADERS:
      return {};
    default:
      return state;
  }
}

const updateHeaders = (state, action)=>{
  const newState = { ...state, ...action.value };
  return newState;
}
