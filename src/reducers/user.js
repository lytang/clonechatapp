 
import {
	GET_ALL_USERINFO_REQUEST,
	GET_ALL_USERINFO_SUCCESS,
	GET_ALL_USERINFO_FAILURE,
} from '../constants/index';
const initState = {
 userInfo:[],
 isFetching:false,
 error:''
}
const userInfoList = (state = initState, action) => {

  switch(action.type){
    case GET_ALL_USERINFO_REQUEST:
    	return { ...state, isFetching:true, error:''}
    case GET_ALL_USERINFO_SUCCESS:
    	return { ...state, isFetching:false, error:'',userInfo: action.data}
    case GET_ALL_USERINFO_FAILURE:
    	return { ...state, isFetching:false, error:action.error}
    default:
      return state;
  }
}


export default userInfoList