import {
  ADD_CHATROOMS,
  GET_ALL_CHATROOMS,
  GET_ALL_CHATROOMS_SUCCESS,
  GET_ALL_CHATROOMS_REQUEST,
	GET_ALL_CHATROOMS_FAILURE,
} from '../constants/chatRoomList';
import {
  GET_ALL_DIRECTCHATROOMS_SUCCESS,
  GET_ALL_DIRECTCHATROOMS_REQUEST,
  GET_ALL_DIRECTCHATROOMS_FAILURE,
} from '../constants';
const initState = {
 chatRooms:[],
 directChatRooms:[],
 isFetching:false,
 error:''
}
const chatRoomList = (state = initState, action) => {
  switch(action.type){
    case ADD_CHATROOMS:
      return addChatRooms(state, action);
    case GET_ALL_CHATROOMS_SUCCESS:
    	return { ...state,chatRooms: action.data, isFetching:false, error:''}
    case GET_ALL_CHATROOMS_REQUEST:
    	return { ...state, isFetching:true, error:''}
    case GET_ALL_CHATROOMS_FAILURE:
    	return { ...state, isFetching:false, error:action.error}
    case GET_ALL_DIRECTCHATROOMS_SUCCESS:
      return { ...state, isFetching:false, directChatRooms:action.data, error:''}
    default:
      return state;
  }
}

const addChatRooms = (state, action)=>{
  const chatRooms = [...state.chatRooms, ...action.chatRooms];
  return { ...state, chatRooms };
}

export default chatRoomList