import React, { Component } from "react";
import  ImagePicker from 'react-native-image-picker';
import {
  Icon,
  Button
} from "native-base";

import {
  View
} from 'react-native';

import {
  PRIMARY_COLOR
} from '../constants';

import ImagePickerModal from './ImagePickerModal';

const buttonStyle = {
  paddingRight: 10
};


const iconStyle = {
  color: '#fff'
};

export default class ImageButton extends Component {
  constructor(props){
    super(props);
    this.state = {
      opened: false
    }
  }

  onSelectImage(response){

    this.props.onSelectImage(response);
    this.closeModal()
  }

  openModal(){
    this.setState({
      opened: true
    })
  }
  closeModal(){
    this.setState({
      opened: false
    })
  }

  openCamera(){
    ImagePicker.launchCamera({
      quality: 0.5
    }, (response)  => {
      this.props.onSelectImage(response)
    });

  }

  openGallery(){
    ImagePicker.launchImageLibrary({
      quality: 0.5
    }, (response)  => {
      this.props.onSelectImage(response)
    });
  }


  render(){
    return <View style={{flexDirection: 'row'}}>
     {/* <ImagePickerModal
        visible={this.state.opened}
        onSelectImage={this.onSelectImage.bind(this)}
        onCancel={this.closeModal.bind(this)}
      />*/}
      <Button
        transparent
        iconLeft
        style={buttonStyle}
        onPress={()=>this.openGallery()}
      >
        <Icon name='image' style={iconStyle}/>
      </Button>
      <Button
        transparent
        iconLeft
        style={buttonStyle}
        onPress={()=>this.openCamera()}
      >
        <Icon name='camera' style={iconStyle}/>
      </Button>
    </View>
  }
}
