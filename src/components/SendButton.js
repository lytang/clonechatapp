import React, { Component } from "react";

import {
  Icon,
  Button
} from "native-base";

const buttonStyle = {
  paddingRight: 10
};

const iconStyle = {
  color: '#fff'
};

export default class SendButton extends Component {
  render(){
    return <Button
      transparent
      iconLeft
      style={buttonStyle}
      onPress={() => this.props.onSend({text: this.props.text}, true)}
    >
      <Icon name='send' style={iconStyle}/>
    </Button>
  }
}
