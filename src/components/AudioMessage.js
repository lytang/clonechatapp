import React, { Component } from "react";
import * as Progress from 'react-native-progress';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import PropTypes from 'prop-types';
import {
  Badge
} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  PRIMARY_COLOR
} from '../constants';
import Sound from 'react-native-sound';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

export default class AudioMessage extends Component {
  constructor(props){
    super(props);
    this.state = {
      playedDuration: 0,
      playing: false,
      hasPlayed:this.props.currentMessage.read||false,
    }
  }


  resetPlayDuration(currentMessage){
    if(Number(this.state.playedDuration.toFixed(2)) >= currentMessage.duration){
      this.setState({ playedDuration: 0 })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.currentMessage.isPlaying == true && nextProps.currentMessage.isPlaying == false){
      this.pauseSound()
    }
  }

  playSound(){
    const { currentMessage } = this.props;
    this.props.setPlayingAudio(currentMessage);
    this.resetPlayDuration(currentMessage);
    this.setState({ playing: true });
    this.sound = new Sound(currentMessage.audio, '', (error)=>{
      this.intervalPlayer = setInterval(()=> {
        this.setState({
          playedDuration: this.state.playedDuration + 0.1
        })
        if (Number(this.state.playedDuration.toFixed(2)) >= currentMessage.duration) {
          clearInterval(this.intervalPlayer)
          this.setState({ playing: false })
          this.props.setPlayingAudio({});
        }
      }, 100);

      // this.sound.play();
        // alert('test')
        // console.log('test')
      this.sound.play((success) => {
        console.log('test',success)
        // alert('test')
        if (success) {
          this.setState({hasPlayed:true})
          console.log('test')
          // alert(JSON.stringify('test'))
          // alert(JSON.stringify(success))
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
          // reset the player to its uninitialized state (android only)
          // this is the only option to recover after an error occured and use the player again
          this.sound.reset();
        }
      });
      this.props.onStart(currentMessage);
      this.sound.stop(()=>{
        this.props.readMessages(currentMessage)
      })
    })
  }

  getIconName() {
    if(this.state.playing) {
      return 'pause-circle-filled'
    } else {
      return 'play-circle-filled'
    }
  }

  pauseSound() {
    this.sound.pause();
    clearInterval(this.intervalPlayer);
    this.setState({ playing: false })
  }

  render(){
    const { position, currentMessage } = this.props;
    const { playedDuration } = this.state;
    const progressBar = progressBarStyle[position]
    const icon = iconStyle[position]
    if (typeof currentMessage.duration == 'string') currentMessage.duration = Number(currentMessage.duration)
    const durationAudio = moment.duration(currentMessage.duration, 'seconds').format()
    return <View style={styles.container}>
      <TouchableOpacity
        style={styles.actionButton}
        onPress={()=>{
          this.state.playing? this.pauseSound() : this.playSound()
        }}
      >
        <Icon name={this.getIconName()} color={this.state.hasPlayed?icon.color:this.props.notPlayColor } size={32}/>
      </TouchableOpacity>
      <View style={styles.progressContainer}>
        <Progress.Bar
          color={progressBar.color}
          progress={playedDuration/(currentMessage.duration||1)}
          borderColor={progressBar.borderColor}
        />
      </View>
      <View style={styles.durationContainer}>
        <Text style={{ color: 'white' }}>{durationAudio}</Text>
      </View>
    </View>
  }
}


const progressBarStyle = {
  left: {
    color: 'black',
    borderColor: 'black',
  },
  right: {
    color: 'white',
    borderColor: 'white',
  }
}

const styles = {
  container: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    flexDirection: 'row'
  },
  actionButton: {
    paddingRight: 5,
  },
  progressContainer: {
    paddingTop: 12,
    paddingBottom: 12,
  },
  durationContainer: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 5,
  }
}


const iconStyle = {
  left: {
    color: 'black'
  },
  right: {
    color: 'white'
  }
}



AudioMessage.defaultProps = {
  position: 'left',
  currentMessage: {
    audio: '',
  },
};

AudioMessage.propTypes = {
  position: PropTypes.oneOf(['left', 'right']),
  currentMessage: PropTypes.object,
};
