import React, { Component } from "react";

import {
  GiftedChat,
  Bubble,
  InputToolbar,
  Composer
} from "react-native-gifted-chat";

import {
  PRIMARY_COLOR
} from '../constants';

import {
  SendButton,
  AudioButton,
  ImageButton,
  AudioMessage,
  MessageImage,
  MessageTicks,
} from './index';


const bubbleWrapperStyle = {
  left: {
    backgroundColor: '#fff',
    borderRadius: 5
  },
  right: {
    backgroundColor: PRIMARY_COLOR,
    borderRadius: 5
  },
};

const inputContainerStyle = {
  backgroundColor: PRIMARY_COLOR
};


export default class CustomGiftedChat extends Component {

  renderInputToolbar(inputProps){
    return <InputToolbar
      {...inputProps}
      containerStyle={inputContainerStyle}
    />
  }

  renderSendButton(sendProps){
    return <SendButton
      {...sendProps}
    />
  }

  renderAudioButton(sendProps){
    return <AudioButton
      {...sendProps}
      onStopRecord={this.props.onStopRecord}
      onStartRecord={this.props.onStartRecord}
      sendAudio={this.props.sendAudio}
    />
  }

  renderSend(sendProps){
    if(sendProps.text == ''){
      return this.renderAudioButton(sendProps);
    }else{
      return this.renderSendButton(sendProps);
    }
  }


  renderBubble(bubbleProps){
    return <Bubble {...bubbleProps}
      wrapperStyle={bubbleWrapperStyle}
    />
  }
  render(){
    return <GiftedChat
      messages={this.props.messages}
      user={this.props.user}
      renderBubble={this.renderBubble.bind(this)}
      renderInputToolbar={this.renderInputToolbar.bind(this)}
      renderSend={this.renderSend.bind(this)}
    />
  }
}
