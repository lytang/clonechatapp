import React, { Component } from "react";
import {
  Icon,
  Button
} from "native-base";

const buttonStyle = {
  paddingRight: 10,
  zIndex: 1001
};

const iconStyle = {
  color: '#fff',
};

export default class AudioButton extends Component {
  constructor(props){
    super(props);
    this.state ={
      isRecording:false
    }
  }
  handlePress(){
    const audio = 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3';
    this.props.sendAudio(audio);
  }

  handlePressIn() {
    // alert(!this.state.isRecording)
    if(!this.state.isRecording){
      this.props.onStartRecord()
    }else{
      this.props.onStopRecord()
    }
    this.setState({isRecording:!this.state.isRecording})
    // alert(this.state.isRecording)
    // this.props.onStartRecord()
  }

  handlePressOut() {
    // this.props.onStopRecord()
  }

  render(){
    return <Button
      transparent
      iconLeft
      style={buttonStyle}
      onPress={()=>{this.handlePressIn()}}
      // onPressIn={()=>this.handlePressIn()}
      // onPressOut={()=>this.handlePressOut()}
    >
      <Icon name='mic' style={iconStyle}/>
    </Button>
  }
}
