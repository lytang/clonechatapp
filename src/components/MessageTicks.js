import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
} from 'react-native';

const styles = {
  tick: {
    fontSize: 10,
    backgroundColor: 'transparent',
    color: '#fff',
  },
  tickView: {
    flexDirection: 'row',
    marginRight: 10,
  }
}

export default class MessageTicks extends Component {

  renderSending(){

    return <View style={styles.tickView}>
      <ActivityIndicator
        color='#fff'
        size={10}
      />
    </View>
  }

  renderTicks(){
    const { currentMessage } = this.props;

    return (
      <View style={styles.tickView}>
        {currentMessage.sent && <Text style={styles.tick}>✓</Text>}
        {currentMessage.read && <Text style={styles.tick}>✓</Text>}
      </View>
    );
  }

  render(){
    const { headers, currentMessage,updateSendStatus } = this.props;
    const { currentUserId } = headers;

      // updateSendStatus();
    if (currentMessage.user._id !== currentUserId) {
      return null;
    }

    if (currentMessage.sent || currentMessage.read) {
      // updateSendStaus();
      return this.renderTicks()
    } else if (!currentMessage.sent) {
      // updateSendStaus();
      return this.renderSending();
    }
    // return (
    //   <View style={styles.tickView}>
    //   {currentMessage.sent || currentMessage.read?
    //     <View style={styles.tickView}>
    //       {currentMessage.sent && <Text style={styles.tick}>✓</Text>}
    //       {currentMessage.read && <Text style={styles.tick}>✓</Text>}
    //     </View>:!currentMessage.sent?
    //     <View style={styles.tickView}>
    //       <ActivityIndicator
    //         color='#fff'
    //         size={10}
    //       />
    //     </View>:
    //     <View/>
    //     }
    //   </View>
    //   )
    return null;
  }
}
