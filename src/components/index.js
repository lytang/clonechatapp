import SendButton from './SendButton';
import AudioButton from './AudioButton';
import ImageButton from './ImageButton';
import AudioMessage from './AudioMessage';
import MessageImage from './MessageImage';
import MessageTicks from './MessageTicks';
import CustomGiftedChat from './CustomGiftedChat';

module.exports =  {
  SendButton,
  AudioButton,
  ImageButton,
  AudioMessage,
  MessageImage,
  MessageTicks,
  CustomGiftedChat,
}
