import React, { Component } from "react";

import {
  Icon,
  Button
} from "native-base";

import {
  Modal,
  View,
  Text
} from 'react-native';

import {
  PRIMARY_COLOR
} from '../constants';

import  ImagePicker from 'react-native-image-picker';

const styles = {
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  inner: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    padding: 20,
    backgroundColor: 'white',
    width: '80%',
    height: 250,
  },
  button: {
    backgroundColor: PRIMARY_COLOR,
    borderRadius: 25,
    marginVertical: 5,
    paddingVertical: 10,
  },
  cancelButton: {
    borderRadius: 25,
    marginVertical: 5,
    paddingVertical: 10,
  },
  iconStyle: {
    color: '#fff'
  },
  buttonText: {
    color: '#fff',
    marginHorizontal: 10
  },
  title: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'center'
  }
}

export default class ImagePickerModal extends Component {

  openCamera(){
    ImagePicker.launchCamera({
      quality: 0.5
    }, (response)  => {
      this.props.onSelectImage(response)
    });

  }

  openGallery(){
    ImagePicker.launchImageLibrary({
      quality: 0.5
    }, (response)  => {
      this.props.onSelectImage(response)
    });
  }

  renderGalleryButton(){
    return <Button
      block
      iconLeft
      style={styles.button}
      onPress={()=>this.openGallery()}
    >
      <Icon name='image' style={styles.iconStyle}/>
      <Text style={styles.buttonText}>From Gallery</Text>
    </Button>
  }

  renderCameraButton(){
    return <Button
      block
      iconLeft
      style={styles.button}
      onPress={()=>this.openCamera()}
    >
      <Icon name='camera' style={styles.iconStyle}/>
      <Text style={styles.buttonText}>Take a photo</Text>
    </Button>
  }

  renderCancelButton(){
    return <Button
      block
      danger
      style={styles.cancelButton}
      onPress={()=>this.props.onCancel()}
    >
      <Text style={styles.buttonText}>Cancel</Text>
    </Button>
  }

  render(){
    return <Modal
      animationType="fade"
      onRequestClose={() => {}}
      transparent={true}
      {...this.props}
    >
      <View style={styles.wrapper}
      >
        <View style={styles.inner}>
          <Text style={styles.title}>Select Image</Text>
          {this.renderGalleryButton()}
          {this.renderCameraButton()}
          {this.renderCancelButton()}
        </View>
      </View>
    </Modal>
  }
}
