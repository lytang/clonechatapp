const Realm = require('realm');
const DEFAULT_AVATAR = require("../images/default-avatar.png");
const MessageSchema = {
  name: 'Message',
  properties: {
    id:  'integer',
    message: 'string',
    user_id: 'integer',
    user_name: 'string',
    user_img: 'string',
    group_id: 'integer',
    type: 'integer',
    createdAt: message.created_at,
    sent: 'integer',
    read: 'integer',
    view: 'integer',
        // user: {
        //   _id: parseInt(message.user_id),
        //   avatar: DEFAULT_AVATAR
        // }
  }
};
// export function addAllMessages(messages){
//   messages.map((message,index)=>{
//     addMessages(message)
//   })
// }
export function addMessages (messages){
	const groupData = Realm.open({schema: [Message]}).then(realm=>{
		messages.map((message,index)=>{
    // addMessages(message)
    realm.write(() => {
      const group = realm.create('Message', {
        id: parseInt(message.id),
        message: message.message,
        user:{
          _id:parseInt(message.user_id),
          avatar: DEFAULT_AVATAR
        }
        group_id: message.group_id,
        type: message.types,
        createdAt: message.created_at,
        sent: message.sent,
        read: message.read,
        view: message.view
      });
    });
  })

	})

}

export function getMessageByGroup(group_id){
	const groupData = Realm.open({schema: [Message]}).then(realm=>{
	const allGroupData = realm.objects('Message').filtered("Group.group_id = $0",group_id);
		// realm.get(() => {
  //     const group = realm.create('Group', {
  //       id: data.id,
  //       name: data.name
  //     });
  //   });
  // realm.close();
	})
}

// export function 