const Realm = require('realm');

export const UserSchema = {
  name: 'UserInfo',
  properties: {
    id:  'int',
    name: 'string',
    types: 'string',
    mobile: 'string'
  }
};

// const UserSchema = {
//   name: 'User',
//   properties: {
//     id: 'int',
//     imageUrl: {type:'string', optional: true},
//     username:  'string',
//     email: {type:'string',optional:true},
//     firstname: {type:'string',optional:true},
//     lastname: {type:'string',optional:true},
//     phone: {type:'string',optional:true},
//     address: {type:'string',optional:true},
//     zipcode: {type:'string',optional:true},
//     city: {type:'string',optional:true},
//     country: {type:'string',optional:true},
//     password: 'string',
//   }
// };

export function getData (typeData,users){
  Realm.open({schema: [UserSchema]})
  .then(realm=>{
    switch(typeData){
      case 'add': addUser(realm,users);break;
      case 'list': let data = listAllUser(realm);return data[0];
      default: return ;
    }
    
  }).catch((error)=>alert(JSON.stringify(error)))
}

export function addUser (realm,users){
  // users.map((user,index)=>{
  //   alert(JSON.stringify(user))
  // })
    users.map((user,index)=>{
  		realm.write(() => {
        const userData = realm.create('UserInfo', {
          id: user.id,
          name: user.name,
          types: user.types,
          mobile: user.mobile
        });
      });
    })
	

}
// export function listAllUser(realm){
//   // let data = [{name:'test'}];
//   // return data;
//   // Realm.open({schema: [UserSchema]}).then(realm=>{
//   let allUserData = realm.objects('UserInfo');
//   // let data = [{name:'test'}];
//   // return data;
//   // if(allUserData.length>0) {
//     // alert(JSON.stringify(allUserData))
//     // return (allUserData)
//     // }
//     // alert(JSON.stringify('no data'))
//     //  allUserData = []
//     return (allUserData)
//   // return allUserData;
//   // }).catch((err)=> {console.log(err);reject(err)})

// }

export const listAllUser = (realm) => new Promise((resolve, reject)=>{
  // Realm.open({schema: [Collection]})
  // .then(realm => {
    let data = realm.objects('UserInfo');
    // alert('data: '+JSON.stringify(data))
    resolve (data)
  // }).catch((err)=> {console.log(err);reject(err)})
  })



// export const listAllUser = () => new Promise((resolve, reject)=>{
//   let data = [{name:'test'}];
//   return data;
 //  Realm.open({schema: [UserSchema]}).then(realm=>{
	// let allUserData = realm.objects('UserInfo');
 //  let data = [{name:'test'}];
 //  return data;
 //  if(allUserData.length>0) {
 //    // alert(JSON.stringify(allUserData))
 //    return (allUserData)
 //    }
 //     allUserData = []
 //    return (allUserData)
 //  // return allUserData;
	// }).catch((err)=> {console.log(err);reject(err)})
// })

// export function 