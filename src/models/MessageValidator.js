export default class MessageValidator {
  constructor(message){
    this.message = message
  }

  isValid(){
    console.log('this.isValidObject()', this.isValidObject())

    console.log('this.isValidType()', this.isValidType())
    return this.isValidObject() && this.isValidType();
  }

  isValidType(){
    const type = parseInt(this.message.types)
    return [0, 1, 2].includes(type)
  }

  isValidObject(){
    const message    = this.message;
    const hasTypes   = message.hasOwnProperty('types')
    const hasMessage = message.hasOwnProperty('message')
    const hasCreatedAt = message.hasOwnProperty('created_at')
    console.log('message', message)
    console.log('hasTypes', hasTypes)
    console.log('hasMessage', hasMessage)
    console.log('hasCreatedAt', hasCreatedAt)
    return hasTypes && hasMessage && hasCreatedAt
  }

  isInvalid(){
    !this.isValid()
  }
}
