import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';
import { persistStore, persistCombineReducers } from 'redux-persist';
import Reactotron from 'reactotron-react-native'
// import '../ReactotronConfig'
const initialState = {
  headers: {},
  login: {},
  chatRoomList: {
    chatRooms: []
  },
  // messages: {},
}
export default function configureStore() {
	// let store = null;	
	// if (__DEV__) {
 //    store = Reactotron.createStore(reducers, {}, applyMiddleware(thunk));
 //  }
 //  else {
 //    store = compose(applyMiddleware(thunk))(createStore)(reducers);
 //  }
  let store = createStore(reducers,applyMiddleware(thunk)) 

  // const store = createStore(reducers, initialState);
  const persistor = persistStore(store)
  return { persistor, store };
}