import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  Keyboard,
  Vibration,
  Alert,
  BackHandler
} from "react-native";
import _ from 'lodash';
import {
  Button,
  Toast,
} from "native-base";
import {
  PRIMARY_COLOR
} from '../constants';
import ImagePicker from 'react-native-image-picker';
import {getAllMessages} from '../actions/messagesList';
import {
  SendButton,
  AudioButton,
  ImageButton,
  AudioMessage,
  MessageImage,
  MessageTicks,
  CustomGiftedChat
} from '../components';

import {
  MessageValidator
} from '../models'

import {
  AudioRecorder,
  AudioUtils
} from 'react-native-audio';

import {
  GiftedChat,
  Bubble,
  InputToolbar,
  Composer
} from "react-native-gifted-chat";
import {getAllChatRooms} from '../actions/chatRoomList';
import SocketIOClient from 'socket.io-client';
import Icon from 'react-native-vector-icons/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { NavigationActions, StackActions } from 'react-navigation';
import {apiUrl} from '../constants/apiUrl'
import {connect} from 'react-redux';
import {getUserInfoOffline} from '../actions/user'
import * as UserModel from '../models/user';
const bubbleWrapperStyle = {
  left: {
    backgroundColor: '#fff',
    borderRadius: 5
  },
  right: {
    backgroundColor: PRIMARY_COLOR,
    borderRadius: 5
  },
  notreadright:{
    backgroundColor: '#3700B3',
    borderRadius: 5
  }
};

const inputContainerStyle = {
  backgroundColor: PRIMARY_COLOR
}

const textInputStyle = {
  color: '#fff'
}

const DEFAULT_AVATAR = require("../images/default-avatar.png");

const guid = ()=>{
  const s4 = ()=>{
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
const styles = {
  tick: {
    fontSize: 10,
    backgroundColor: 'transparent',
    color: '#fff',
  },
  tickView: {
    flexDirection: 'row',
    marginRight: 10,
  }
}

class ChatRoom extends Component {
  static navigationOptions = ({ navigation, screenProps }) =>({
    headerRight: <Button transparent onPress={()=>navigation.navigate('Detail', {chatRoom: navigation.state.params.chatRoom})}><Icon name="bars" size={20} style={{color: '#fff', paddingRight: 20, paddingTop: 10}} /></Button>,
    title: navigation.state.params.chatRoom.name,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: PRIMARY_COLOR,
    },
    headerTitleStyle: { alignSelf: 'center' },
    headerLeft:<Button transparent onPress={()=>navigation.goBack()}><FeatherIcon name="arrow-left" size={30} style={{color: '#fff', paddingLeft: 20, paddingTop: 10}} /></Button>, 
  });

  constructor(props){
    super(props);

    this.params = props.navigation.state.params;
    this.state = {
      messages: [],
      fetching: true,
      isRecording: false,
      recordingDuration: 0,
      hasPermission: undefined,
      playingAudio: {},
      audioFileURL: {},
      lastMessageId:null,
      loading:false,
      userInfo:[]
    }

    // this.socket = SocketIOClient('http://bchetthor.thadatra.com:3000');
    this.socket = SocketIOClient('http://localhost:9000')
  }

  async componentWillMount() {
    // const { chatRoom } = this.params;
    // const { messages } = this.state;
    // const { headers } = this.props;
    // let formdata = new FormData();
    this.props.navigation.setParams({ backButton: ()=>this._handleBackPress.bind(this) });
    // alert(JSON.stringify(UserModel.getData('list')))
    this.getMessages();
    this.subscribeChannel();
    // this.props.getUserInfoOffline();
    // let userInfo =await UserModel.getData('list');
    // this.setState({userInfo:userInfo})
    // this.props.getAllMessages(headers,chatRoom.id,'',messages.length);
    BackHandler.addEventListener('hardwareBackPress', this._handleBackPress.bind(this));
  }
  

  componentWillUnmount(){
    this.socket.close();
    // this.backHandler.remove();
    BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress.bind(this));
  }

  componentDidMount() {
    // this.subscribeChannel();
    this._checkPermission().then((hasPermission) => {
      this.setState({ hasPermission });

      if (!hasPermission) return;

      AudioRecorder.onProgress = (data) => {
        this.setState({recordingDuration: Math.floor(data.currentTime)});
      };
    });
  }

  _handleBackPress = () => {
        this.viewMessages(this.state.messages);
        this.props.getAllChatRooms(this.props.headers)
        this.props.navigation.goBack();
        return true;
    }

  subscribeChannel(){
    const { headers } = this.props
    const { apitoken, currentUserId } = headers;


    this.socket.on('connect', function(){
      alert('connect')
      console.log('connected')
    });

    this.socket.on('disconnect', function(){
      console.log('disconnect')
    });

    this.socket.emit('adduser', {
      'user_id' : currentUserId,
      'token' : apitoken
    });

    this.socket.on('messages', (data) => {
      alert('message')
      // alert('messages received '+JSON.stringify(data))
      console.log('message: ',data)
      const { headers } = this.props;
      const { currentUserId } = headers;
      const { user_id, group_id } = data;
      const isBelong = user_id == currentUserId;
      const isInGroup = parseInt(group_id) == this.params.chatRoom.id
      if(!isBelong && isInGroup ){
        const formatedMessage = this.formatMessage(data);
        this.setState({ messages: [formatedMessage, ...this.state.messages] });
        if(formatedMessage.types != 2){ // voice read when play
          this.readMessages(formatedMessage);
        }
        this.getMessages();
        this.props.getAllChatRooms(this.props.headers);
      }
    });
  }

  setPlayingAudio(message){
    const messages = this.state.messages.map((m)=>{
      if(m._id == message._id){
        m.isPlaying = true;
      }else{
        m.isPlaying = false;
      }
      return m;
    })
    this.setState({messages: [...messages]})
  }

  getMoreMessages(){
    const { fetchable, fetching } = this.state;
    if (!fetching && fetchable) {
      this.setState({ fetching: true }, ()=>{
        this.getMessages()
      })
    }

  }

  getMessages(){
    const { chatRoom } = this.params;
    const { messages } = this.state;
    const { headers } = this.props;
    let formdata = new FormData();
    formdata.append("group_id", ''+chatRoom.id)
    formdata.append("count", ''+messages.length)
    // console.log('state: ', messages);
    // const url = 'http://bchetthor.thadatra.com/api/messages-list';
    // alert('formData: '+JSON.stringify(messages))
    const url = `${apiUrl}/api/messages-list`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formdata
    }).then((response)=>{
      // console.log(response.json())
      if(this.isValidResponse(response)){
        return response.json();
      }

    }).then((messagesData)=>{
      // console.log('new: ', messagesData)
      // alert('messagesData: '+JSON.stringify(messagesData))
      if(messagesData.length > 0){
        const formatedMessages = messagesData.map((message)=>(this.formatMessage(message)));
        this.setState({
          messages: [...this.state.messages, ...formatedMessages],
          fetching: false,
          fetchable: true
        })
        const notVoiceMessages = formatedMessages.filter((message)=>(message.types != 2));
        this.viewMessages(formatedMessages);
        this.readMessages(notVoiceMessages);
        // alert(JSON.stringify(notVoiceMessages))
      } else {
        this.setState({
          fetching: false,
          fetchable: false
        })
      }
    })
  }

  viewMessages(messages){
    if(_.isEmpty(messages)){
      messages = this.state.messages
    }
    const unviewMessages = _.filter(messages,function(message){return !message.view})
    // const unreadMessages = messages.filter((message)=>{alert(JSON.stringify(message);return (message.read==false)})
    const { chatRoom } = this.params;
    const groupId=chatRoom.id;
    let unreadMessageIds = unviewMessages.map((message)=>message._id);
    if(unviewMessages.length < 1){ return };
    const { headers } = this.props;
    const url = `${apiUrl}/api/message-view`;
    // const url = 'http://bchetthor.thadatra.com/api/message-status';
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        messages: unreadMessageIds,//JSON.stringify(array_ids)}
        group_id: groupId
      }) 
    }).then((data)=>{
      // alert(JSON.stringify(unviewMessages))
      const { apitoken } = this.props.headers;
      // this.reloadChatRoom()
      this.getMessages();
      this.props.getAllChatRooms(headers)
      // alert(JSON.stringify(data))
    })
  }
//to do need to write action to get message again
  readMessages(messages){
    const unreadMessages = _.filter(messages,function(message){return !message.read})
    // const unreadMessages = messages.filter((message)=>{return (!message.read)})
    const { chatRoom } = this.params;
    const groupId=chatRoom.id;
    let unreadMessageIds = unreadMessages.map((message)=>message._id);
    if(unreadMessages.length < 1){ return };
    const { headers } = this.props;
    const url = `${apiUrl}/api/message-status`;
    // const url = 'http://bchetthor.thadatra.com/api/message-status';
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        messages: unreadMessageIds,//JSON.stringify(array_ids)}
        group_id: groupId
      }) 
    }).then((data)=>{
      // this.reloadChatRoom()
      // alert(JSON.stringify(data))
      if(data.status ==200){
        return (data.json())
      }
     
      // alert(JSON.stringify(data))
    }).then((messages)=>{
       this.getMessages();
      // alert(JSON.stringify(messages))
      // alert(JSON.stringify(messages))
      // this.viewMessages(messages);
      
      this.setState({loading:!this.state.loading})
    })
  }

  formatMessage(message){
    if(message.types == 0){ // text message
      return {
        _id: parseInt(message.id),
        text: message.message,
        createdAt: message.created_at,
        sent: true,
        types: message.types,
        read: message.read == 1,
        view: message.view == 1,
        user: {
          _id: parseInt(message.user_id),
          // name: message.username,
          avatar: DEFAULT_AVATAR
        }
      }
    }else if(message.types == 1){ // image message
      return {
        _id: parseInt(message.id),
        image: message.message,
        createdAt: message.created_at,
        types: message.types,
        sent: true,
        read: message.read == 1,
        view: message.view == 1,
        user: {
          _id: parseInt(message.user_id),
          // name: message.username,
          avatar: DEFAULT_AVATAR
        }
      }
    } else if (message.types == 2) { // voice message
      return {
        _id: parseInt(message.id),
        audio: message.message,
        createdAt: message.created_at,
        types: message.types,
        duration: message.duration,
        sent: true,
        read: message.read == 1,
        view: message.view == 1,
        user: {
          _id: parseInt(message.user_id),
          // name: message.username,
          avatar: DEFAULT_AVATAR
        }
      }
    }
  }

  onSend(messages = []) {
    messages.map((message, index) => {
      this.handleSendText(message)
    })
    this.getMessages();
    // this.reloadChatRoom()
  }

  handleSendText(message) {
    const { headers } = this.props;
    const { currentUserId } = headers;
    const { chatRoom } = this.params;
    if(message.text != ''){
      const url = `${apiUrl}/api/message`;
      // const url = 'http://bchetthor.thadatra.com/api/message';
      const uuid = this.appendSendingText(message.text);
      const data = new FormData();
      data.append('message', message.text)
      data.append('group_id', chatRoom.id);
      data.append('types', 0);
      // data.append('member_id', currentUserId);
      if (this.params.member) {
        data.append('member_id', this.params.member.id);
      }
      // console.log('headers', headers);
      // console.log('data', data);
      fetch(url, {
        method: 'POST',
        headers: {
          ...headers,
          'apiuser': currentUserId,
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body: data
      })
      .then((response)=>{
        // console.log(response)
        if(this.isValidResponse(response)){
          return response.json()
        }
      })
      .then((data)=>{
        if(this.isValidMessage(data.messeage)){
          const formatedMessage = this.formatMessage(data.messeage)
          let messages = [...this.state.messages];
          const index = messages.findIndex((message)=>(message._id == uuid))

          if(index > -1){
            messages[index] = formatedMessage;
            // alert(JSON.stringify(formatedMessage))
            this.setState({ messages: messages })
          }else{
            this.setState({ messages: [formatedMessage, ...this.state.messages] })
          }
        }
      })
    }

  }

  // custom gift chat
  renderBubble(bubbleProps){
//     const bubbleWrapperStyle = {
//   left: {
//     backgroundColor: '#fff',
//     borderRadius: 5
//   },
//   readright: {
//     backgroundColor: PRIMARY_COLOR,
//     borderRadius: 5
//   },
//   notreadright:{
//     backgroundColor: 'green',
//     borderRadius: 5
//   }
// };
  // alert(JSON.stringify(bubbleProps))
  // console.log(bubbleProps)
    return <Bubble {...bubbleProps}
      wrapperStyle={bubbleWrapperStyle}
      backgroundColorStyle ={{backgroundColor: '#018786',borderRadius: 5}}
    />
  }

  renderInputToolbar(inputProps){
    return <InputToolbar
      {...inputProps}
      containerStyle={inputContainerStyle}
    />
  }

  renderSendButton(sendProps){
    return <SendButton
      {...sendProps}
    />
  }

  renderAudioButton(sendProps){
    return <AudioButton
      {...sendProps}
      onStopRecord={this.onStopRecord.bind(this)}
      onStartRecord={this.onStartRecord.bind(this)}
      sendAudio={this.sendAudio.bind(this)}
    />
  }

  renderSend(sendProps){
    if(sendProps.text == ''){
      return this.renderAudioButton(sendProps);
    }else{

    return this.renderSendButton(sendProps);
    }
  }

  renderComposer(composerProps){
    return <Composer
      {...composerProps}
      textInputStyle={textInputStyle}
    />
  }

  appendSendingText(text){
    const uuid = guid()
    const { headers } = this.props
    const { currentUserId } = headers
    const message = {
      _id: uuid,
      text: text,
      createdAt: new Date(),
      types: 0,
      read: false,
      user: {
        _id: currentUserId,
        avatar: DEFAULT_AVATAR
      }
    }
    this.setState({ messages: [message, ...this.state.messages] })
    this.readMessages(message);
    this.getMessages();
    return uuid;
  }

  isValidMessage(message){
    const validator = new MessageValidator(message)
    // console.log(validator.isValid())
    if(validator.isValid()){
      return true
    }else{
      Alert.alert('Sorry', 'Server response with invalid object \n we need to reload your app', [
        {text: 'Reload', onPress: () => this.reloadChatRoom()},
      ])
    }
  }

  isValidResponse(response){
    const status = parseInt(response.status)
    if(status >= 200 && status < 300){
      return true
    }else{
      Alert.alert('Sorry', `Server response with status errors ${status} \n we need to reload your app`, [
        {text: 'Reload', onPress: () => this.reloadChatRoom()},
      ])
    }

  }

  reloadChatRoom(){
    // const resetAction = StackActions.reset({
    //   index: 1,
    //   actions: [
    //     NavigationActions.navigate({ routeName: 'Listing' }),
    //     NavigationActions.navigate({
    //       routeName: 'ChatRoom',
    //       params: {
    //         chatRoom: this.params.chatRoom
    //       }
    //     })
    //   ]
    // })
    // this.props.navigation.dispatch(resetAction);

    // this.props.navigation.dispatch(resetAction)
    // need implement
  }

  appendSendingImage(image){
    const uuid = guid()
    const { headers } = this.props
    const { currentUserId } = headers
    // to do you have to find another way to make sender read
    const message = {
      _id: uuid,
      image: image.uri,
      createdAt: new Date(),
      types: 1,
      read: true,
      user: {
        _id: currentUserId,
        avatar: DEFAULT_AVATAR
      }
    }

    this.setState({ messages: [message, ...this.state.messages] })
    // this.props.getAllMessages();
    // alert(JSON.stringify(message))
    this.readMessages(message)
    // this.getMessages();
    // alert(JSON.stringify(message))
    return uuid;
  }

  appendSendingAudio(audioPath) {
    const uuid = guid()
    const { headers } = this.props
    const { currentUserId } = headers
    const message = {
      _id: uuid,
      audio: audioPath,
      duration: this.state.recordingDuration,
      createdAt: new Date(),
      types: 2,
      read: false,
      user: {
        _id: currentUserId,
        avatar: DEFAULT_AVATAR
      }
    }
    this.setState({ messages: [message, ...this.state.messages] })
    // this.props.getAllMessages();
    // this.getMessages()
    this.readMessages(message);
    return uuid;
  }

  onSelectImage(image){
    if(!image.didCancel){

    const { headers } = this.props;
    const { currentUserId } = headers;
    const { chatRoom } = this.params;
    const url = `${apiUrl}/api/message`;
    const uuid = this.appendSendingImage(image);
    const data = new FormData();
    data.append('apiimage', {
      uri: image.uri,
      type: image.type, // or photo.type
      name: image.fileName
    })
    data.append('group_id', chatRoom.id);
    data.append('types', 1);
    if (this.params.member) {
      data.append('member_id', this.params.member.id);
    }

    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'apiuser': currentUserId,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
    .then((response)=>{
      // alert(this.isValidResponse(response))
      if(this.isValidResponse(response)){
        return response.json()
      }
    })
    .then((data)=>{
      // console.log('data')
      // console.log(data)
      // alert(JSON.stringify(this.isValidMessage(formatedMessage)))
      if(this.isValidMessage(data.messeage)){
        const formatedMessage = this.formatMessage(data.messeage)
        let messages = [...this.state.messages];
        const index = messages.findIndex((message)=>(message._id == uuid))
        // console.log(message)
        if(index > -1){
          messages[index] = formatedMessage;
          this.setState({ messages: messages })
          // this.readMessages(formatedMessage);
        }else{
          // const allMessages = [formatedMessage, ...this.state.messages];
          this.setState({ messages: [formatedMessage, ...this.state.messages] })
        }
        // this.reloadChatRoom();
          // this.readMessages(formatedMessage);
        this.getMessages()
      }
    })
    }
  }

  sendAudio(audioPath){
    const { headers } = this.props;
    const { currentUserId } = headers;
    const { chatRoom } = this.params;
    const url = `${apiUrl}/api/message`;
    const uuid = this.appendSendingAudio(audioPath);
    const data = new FormData();
    data.append('apiimage', {
      uri: "file://" + audioPath,
      type: "audio/aac",
      name: audioPath.substring(audioPath.lastIndexOf("/"))
    })
    data.append('group_id', chatRoom.id);
    data.append('types', 2);
    if (this.params.member) {
      data.append('member_id', this.params.member.id);
    }
    data.append('duration', this.state.recordingDuration);

    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'apiuser': currentUserId,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
    .then((response)=>{
      if(this.isValidResponse(response)){
        return response.json()
      }
    })
    .then((data)=>{
      if(this.isValidMessage(data.messeage)){
        const formatedMessage = this.formatMessage(data.messeage)
        let messages = [...this.state.messages];
        const index = messages.findIndex((message)=>(message._id == uuid))
        if(index > -1){
          messages[index] = formatedMessage;
          this.setState({ messages: messages })
          // this.readMessages(messages);
        }else{
          const allMessages = [formatedMessage, ...this.state.messages];
          this.setState({ messages: [formatedMessage, ...this.state.messages] })
        }
          this.readMessages(formatedMessage);
      }
    })
  }

  prepareRecordingPath(audioPath) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      OutputFormat: 'aac_adts',
    });
  }

  _checkPermission() {
    if (Platform.OS !== 'android') {
      return Promise.resolve(true);
    }

    const rationale = {
      'title': 'Microphone Permission',
      'message': 'LineClone needs access to your microphone so you can record audio.'
    };

    return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale)
      .then((result) => {
        return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
      });
  }

  onStartRecord(){
    if (!this.state.hasPermission) {
      console.warn('Can\'t record, no permission granted!');
      return;
    }
    if(!this.state.isRecording){
      let filename = AudioUtils.DocumentDirectoryPath + '/sparkplant' + Date.now() + '.aac';
      this.prepareRecordingPath(filename);
      this.setState({isRecording: true, recordingDuration: 0});

      setTimeout(() =>{
        try {
          AudioRecorder.startRecording()
        } catch (error) {
          console.log(error);
          AudioRecorder.stopRecording().then(()=>{
            Toast.show({
              text: "Try to record again",
            })
            this.setState({isRecording: false});
          })
        }
      }, 500);
    }
  }


  onStopRecord() {
    AudioRecorder.stopRecording().then((filePath)=>{
      this.setState({isRecording: false});
      this.sendAudio(filePath)
    })
  }

  renderActions(actionsProps){
    return <ImageButton
      {...actionsProps}
      onSelectImage={this.onSelectImage.bind(this)}
    />

  }

  renderCustomView(props){
    const { currentMessage } = props;
    // alert(JSON.stringify( _.find(this.props.userInfo,function(user){return (user.id == currentMessage.user._id})))
    const user = _.find(this.props.userInfo,function(user){return (user.id == currentMessage.user._id)})
    // alert(JSON.stringify(props.currentMessage.user._id))
    console.log('all props ',props)
    if(currentMessage.types == 2){
      return <View>
      {props.currentMessage.user._id != this.props.headers.currentUserId?
          <View/>
        // <Text style={{paddingLeft: 10, color:'rgb(95,111,238)'}}>{user.name}</Text>
        :<View/>
      }      
          <AudioMessage {...props}
        setPlayingAudio={this.setPlayingAudio.bind(this)}
        readMessages={(message)=>this.readMessages(message)}
        onStart={(message)=>{this.readMessages([message])}}
        notPlayColor="#33d099"
      />
      </View>
    }else{
      if(props.currentMessage.user._id != this.props.headers.currentUserId){
        <View/>
        // return <Text style={{paddingLeft: 10, color:'rgb(95,111,238)'}}>{user.name}</Text>;
      }
    }
  }

  renderRecordingTime() {
    if(!this.state.isRecording){return};
    return <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <View style={{backgroundColor: PRIMARY_COLOR, borderRadius:10, marginBottom: 10}}>
        <Text style={{color: '#fff', textAlign: 'center', padding: 10}}>{this.state.recordingDuration}</Text>
      </View>
    </View>
  }

  renderTicks(currentMessage) {
    // alert(JSON.stringify(currentMessage))
    // this.readMessages(currentMessage)
    return (<View style={styles.tickView}>
      {currentMessage.sent || currentMessage.read?
        <View style={styles.tickView}>
          {currentMessage.sent && <Text style={styles.tick}>✓</Text>}
          {currentMessage.read && <Text style={styles.tick}>✓</Text>}
        </View>:!currentMessage.sent?
        <View style={styles.tickView}>
          <Text style={styles.tick}>✓</Text>
        </View>:
        <View/>
        }
      </View>
      )
    // const {updateSendState} = this.state;
    // return <MessageTicks
    //   {...this.props}
    //   updateSendStatus = {()=> this.setState({updateSentState:!updateSendState})}
    //   currentMessage={currentMessage}
    // />
  }


  renderMessageImage(props){
    return <MessageImage {...props} />
  }

  // *************
  render() {
    // console.log(this.params)
    const { headers, messages } = this.props
    const { currentUserId } = headers
    // alert(JSON.stringify(this.props.messages))
    // console.log(messages)
    // console.log(this.state.messages)
    if(this.state.loading){
      return <GiftedChat
      messages={this.state.messages}
      onSend={messages => {this.onSend(messages)}}
      user={{ _id: currentUserId }}
      renderBubble={this.renderBubble.bind(this)}
      renderInputToolbar={this.renderInputToolbar.bind(this)}
      renderSend={this.renderSend.bind(this)}
      renderComposer={this.renderComposer.bind(this)}
      renderCustomView={this.renderCustomView.bind(this)}
      renderActions={this.renderActions.bind(this)}
      renderMessageImage={this.renderMessageImage.bind(this)}
      listViewProps={{
        onEndReached: ()=>this.getMoreMessages(),
      }}
      renderChatFooter={this.renderRecordingTime.bind(this)}
      renderTicks={this.renderTicks.bind(this)}
    />;
    }else{
    return <GiftedChat
      messages={this.state.messages}
      onSend={messages => {this.onSend(messages)}}
      user={{ _id: currentUserId }}
      renderBubble={this.renderBubble.bind(this)}
      renderInputToolbar={this.renderInputToolbar.bind(this)}
      renderSend={this.renderSend.bind(this)}
      renderComposer={this.renderComposer.bind(this)}
      renderCustomView={this.renderCustomView.bind(this)}
      renderActions={this.renderActions.bind(this)}
      renderMessageImage={this.renderMessageImage.bind(this)}
      listViewProps={{
        onEndReached: ()=>this.getMoreMessages(),
      }}
      renderChatFooter={this.renderRecordingTime.bind(this)}
      renderTicks={this.renderTicks.bind(this)}
    />;
  }
  }
}

function mapStateToProps(state){
  return {
    messages: state.messageList.messages,
    userInfo: state.userInfoList.userInfo,
  }
}

function mapDispatchToProps(dispatch){
  return {
    getAllChatRooms: (header)=>(dispatch(getAllChatRooms(header))),
    getAllMessages: (header, groupId, userId, numOfMessages)=> dispatch(getAllMessages(header, groupId, userId, numOfMessages)),
    getUserInfoOffline: ()=>(dispatch(getUserInfoOffline()))
  }
}

export default connect (mapStateToProps, mapDispatchToProps)(ChatRoom)