import React, { Component } from 'react';
import {
  View,
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Body,
  Left,
  Right,
  Item,
  TextInput,
  Input,
  Button,
  Icon
} from "native-base";
import {connect} from 'react-redux';
import {
  StyleSheet,
  RefreshControl
} from "react-native";
// import * as AllAction from '../actions';
import {
  PRIMARY_COLOR
} from '../constants';
import {getAllChatRooms,getAllDirectChatRooms} from '../actions/chatRoomList';
import {apiUrl} from "../constants/apiUrl";
import _ from 'lodash'
import SocketIOClient from 'socket.io-client';
import {getAllUserInfo} from '../actions/user';
// export default 
class Listing extends Component {
  constructor(props){
    super(props);
    this.state = {
      chatRooms: [],
      searchText: '',
      searchedChatRooms: [],
      loading: false,
      directChatRooms:[],
      groupChatRooms:[],
      reload:false,
    }
    this.socket = SocketIOClient('http://192.168.100.101:8000/publish',{transports: ['websocket']});
    // this.socket = SocketIOClient('http://localhost:5000', {transports: ['websocket']}) ;
this.socket.on('connect' , () => {
  alert('connect')
console.log('connected')
})
console.log('constructor')
console.log(this.socket);

  }

  async componentWillMount(){
    const { apitoken, currentUserId } = this.props.headers;
    await this.props.screenProps.setUserToken(apitoken);
    // this.fetchGroupListing();
    // alert('call')
    this.subscribeChannel()
    this.props.getAllUserInfo(this.props.headers)
    this.props.getAllChatRooms(this.props.headers)
    this.props.getAllDirectChatRooms(this.props.headers, currentUserId)
  }
  async componentWillReceiveProps(nextProps){
    if(this.props != nextProps){

      if(!_.isEmpty(nextProps.directChatRooms)){
        let directChats = [];
        nextProps.directChatRooms.map((directChat, index)=>{
          let chatInfo = {...directChat};
          let userInfo = _.find(this.props.userInfo, function(user){
            return user.id == directChat.sender
          })
          if(userInfo){
            chatInfo.name = userInfo.name;
          }
          directChats.push(chatInfo)
        })

        const allChats = [...directChats,...this.state.groupChatRooms];
        console.log('all chats ', allChats)
        this.setState({directChatRooms:directChats})
        // alert('direct chat '+JSON.stringify(allChats))
       await this.setState({chatRooms:allChats});
      }
      if(!_.isEmpty(nextProps.chatRoomsData)){
      // alert(JSON.stringify(nextProps.chatRoomsData))
      const allChats = [...nextProps.chatRoomsData,...this.state.directChatRooms];
        this.setState({chatRooms: allChats, groupChatRooms:nextProps.chatRoomsData})
      }
      // if(nextProps.screenProps.newNotification){
      //   this.props.getAllChatRooms(this.props.headers)
      //   nextProps.screenProps.changeNotificationStatus()
      // }
      // alert(JSON.stringify(this.props.userInfo))
    }

  }

  subscribeChannel(){
    const { headers } = this.props
    const { apitoken, currentUserId } = headers;
    this.socket.on('connect', function(){
      alert('connect')
      console.log('connected')
    });

    this.socket.on('disconnect', function(){
      console.log('disconnect')
    });
    this.socket.on('test-channel',(data)=>{
      alert('test')
    })
    this.socket.on('messages', (data) => {
      const { headers } = this.props;
      const { currentUserId } = headers;
      const { user_id, group_id } = data;
      const isBelong = user_id == currentUserId;
      const isInGroup = parseInt(group_id) == this.params.chatRoom.id
      alert(JSON.stringify('got new data'))
      if(!isBelong && isInGroup ){
        this.props.getAllChatRooms(this.props.headers)
      }
    });
  }
  componentWillUnmount(){
    // this.socket.close();
  }

  subscribeChannel(){
    const { headers } = this.props
    const { apitoken, currentUserId } = headers;


    this.socket.on('connect', function(){
      // alert('connect')
      console.log('connected')
    });

    this.socket.on('disconnect', function(){
      // alert('disconnect')
      console.log('disconnect')
    });

    this.socket.emit('adduser', {
      'user_id' : currentUserId,
      'token' : apitoken
    });

    this.socket.on('test-channel', (data)=>{
      alert('test')
    })

    this.socket.on('messages', (data) => {
      alert(JSON.stringify('get message'))
      const { headers } = this.props;
      // const { currentUserId } = headers;
      // const { user_id, group_id } = data;
      // const isBelong = user_id == currentUserId;

      this.props.getAllChatRooms(this.props.headers);
      
    });
  }



  reloadData(){
    // this.setState({ loading: true })
    const { headers } = this.props;
    const {currentUserId} = headers;
    this.props.getAllChatRooms(this.props.headers)
    this.props.getAllDirectChatRooms(this.props.headers, currentUserId)
  }
  fetchGroupListing() {
    this.setState({ loading: true })
    this.props.getAllChatRooms(this.props.headers)
    const { headers } = this.props;
    const url = `${apiUrl}/api/groups`;
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response)=>{
      return response.json()
    })
    .then((chatRooms)=>{
      console.log(chatRooms)
      // alert(JSON.stringify(chatRooms))
      this.setState({ loading: false })
      this.setState({ chatRooms: chatRooms });
    });
  }

  static navigationOptions = {
    header: null
  }

  goToChatRoom =(chatRoom) => {
    if(chatRoom.sender){
      this.props.navigation.navigate('DirectChatRoom',{
        chatRoom:chatRoom
      })
    }else{
    this.props.navigation.navigate('ChatRoom', {
      chatRoom: chatRoom
    });
      
    }
  }

  renderChatRoom(item){
    console.log('chat list ',item)
    return <ListItem onPress={()=>this.goToChatRoom(item)} avatar>
      <Left>
        <Thumbnail source={require("../images/profile.png")} />
        {/*<View style={styles.status} />*/}
      </Left>
      <Body>
        <Text>{item.name}</Text>
        <Text note>
          Doing what you like will always keep you happy ...
        </Text>
      </Body>
      <Right>
        <Text note>3:43 PM</Text>
        {
          item.nbGroups >0?
        <View style={styles.notification_bg}>
          <Text style={styles.count}>
            {item.nbGroups}
          </Text>
        </View>:<View/>
        }
      </Right>
    </ListItem>

  }

  fetchDirectChat(){
    const {headers} = this.props;
    const {currentUserId} = headers;
    this.props.getAllDirectChatRooms(headers, currentUserId)
  }
  renderRefreshControl(){
    return <RefreshControl
      colors={[PRIMARY_COLOR]}
      refreshing={this.state.loading}
      // onRefresh={()=>{this.fetchGroupListing.bind(this); this.fetchDirectChat.bind(this)}}
      onRefresh={()=>{this.props.getAllChatRooms(this.props.headers);
    this.props.getAllDirectChatRooms(this.props.headers, this.props.headerscurrentUserId)}}
      // onRefresh={this.reloadData.bind(this)}
    />
  }

  chatRoomsSource(){
    if(this.isSearching()){
      return this.state.searchedChatRooms;
    }else{
      return this.state.chatRooms;
    }
  }

  isSearching(){
    return this.state.searchText && this.state.searchText != '';
  }

  isNotSearching(){
    return !this.isSearching();
  }

  handleSearch(searchText){
    const searchedChatRooms = this.state.chatRooms.filter((chatRoom)=>{
      const { name } = chatRoom;
      return name.toLowerCase().includes(searchText.toLowerCase());
    })
    this.setState({
      searchText,
      searchedChatRooms
    })
  }

  render() {
    // const { chatRooms } = this.state;
    // alert(JSON.stringify(this.state.chatRooms))
    const {chatRoomsData} = this.props;
    console.log('chat room state ',this.state.chatRooms)
    // alert(JSON.stringify(this.props)) 
    return <Container style={styles.container}>
        <Header searchBar rounded style={styles.bgheader}>
          <Item>
            <Input
              value={this.state.searchText}
              onChangeText={this.handleSearch.bind(this)}
              style={styles.searchbox}
              placeholder="Search"
              placeholderTextColor="rgba(155,154,155,1.0);"
            />
          <Icon name="search" style={{ color: PRIMARY_COLOR}} />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
        <Content style={styles.content}>
          <List
            refreshControl={this.renderRefreshControl()}
            dataArray={this.chatRoomsSource()}
            enabled={this.isNotSearching()}
            renderRow={this.renderChatRoom.bind(this)}
          />
        </Content>
      </Container>;
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: PRIMARY_COLOR
  },
  content: {
    backgroundColor: "#fff"
  },
  status: {
    backgroundColor: "#90D95C",
    opacity: 1,
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 100,
    width: 12.99,
    height: 12.99,
    position: "absolute",
    top: 0,
    left: 43.53
  },
  notification_bg: {
    backgroundColor: PRIMARY_COLOR,
    opacity: 1,
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 100,
    width: 20.99,
    height: 20.99,
    marginVertical: 5,
    top: 30,
    left: 11,
    position: "absolute"
  },
  count: {
    color: "#fff",
    paddingHorizontal: 6,
    paddingVertical: 1,
    fontSize: 14
  },
  searchbox: {
    borderRadius: 100,
    marginHorizontal: 10
  },
  bgheader: {
    backgroundColor: PRIMARY_COLOR,
  }
});

function mapStateToProps(state){
  return {
    allState: state,
    chatRoomsData: state.chatRoomList.chatRooms,
    userInfo: state.userInfoList.userInfo,
    directChatRooms: state.chatRoomList.directChatRooms,
  }
}

function mapDispatchToProps(dispatch){
  return {
    getAllChatRooms: (header)=>(dispatch(getAllChatRooms(header))),
    getAllUserInfo: (header)=>(dispatch(getAllUserInfo(header))),
    getAllDirectChatRooms: (header, userId)=>(dispatch(getAllDirectChatRooms(header, userId)))
  }
}

export default connect (mapStateToProps, mapDispatchToProps)(Listing)
