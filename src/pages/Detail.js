import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
} from "react-native";

import {
  Button,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
} from "native-base";

import Icon from 'react-native-vector-icons/MaterialIcons';
import { Col, Row, Grid } from "react-native-easy-grid";

import {
  PRIMARY_COLOR
} from '../constants';

export default class Detail extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    title: 'Details',
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: PRIMARY_COLOR,
      elevation: 0,
      shadowOpacity: 0,
    },
    headerTitleStyle: { alignSelf: 'center' },

  });

  constructor(props){
    super(props);
    this.params = props.navigation.state.params;
    this.state = {
      members: []
    }
  }

  componentWillMount() {
    this.getMembers();
  }

  getMembers(){
    const { chatRoom } = this.params;
    const { headers } = this.props;
    let membersList = []
    let formdata = new FormData();
    formdata.append("group_id", ''+chatRoom.id)
    const url = 'http://bchetthor.thadatra.com/api/members';
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formdata
    }).then((response)=>{
      return response.json()
    }).then((members)=>{
      members.forEach((member, index) => {
        membersList[index] = {
          name: member.name,
          photo: require("../images/avatar.png"),
          id: member.id,
        }
      })

      this.setState({members: membersList})
    })
  }

  render() {
    return(
      <ScrollView style={styles.headerContainer}>
        <View>
          <List
            style={styles.list}
            dataArray={this.state.members}
            renderRow={(item) =>
              <ListItem avatar style={styles.listItem}>
                <Left>
                  <Thumbnail source={ item.photo } style={styles.thumbnail} />
                </Left>
                <Body style={styles.noBottomBorder}>
                  <Text style={styles.memberName}>{item.name}</Text>
                </Body>
                <Right style={styles.noBottomBorder}>
                  <Button
                    iconLeft
                    transparent
                    style={{alignSelf: 'center'}}
                    onPress={()=> this.props.navigation.navigate('ChatRoom', { chatRoom: this.params.chatRoom, member: item })}
                  >
                    <Icon name='chat' size={20} color='#fff' />
                  </Button>
                </Right>
              </ListItem>
            }
          />

          <View style={styles.hr}/>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: PRIMARY_COLOR,
    flex: 1
  },
  list: {
    marginTop: 30,
    marginBottom: 20,
    paddingLeft: 35,
    paddingRight: 35
  },
  listItem: {
    marginBottom: -10,
    marginTop: -10,
    paddingTop: 0,
    paddingBottom: 0
  },
  thumbnail: {
    width: 35,
    height: 35,
    borderWidth: 2,
    borderColor: '#fff'
  },
  memberName: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 12
  },
  noBottomBorder: {
    borderBottomWidth: 0
  },
  hr: {
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    marginLeft: 80,
    marginRight: 80
  },
  button: {
    alignSelf: 'center',
    marginBottom: 10,
    marginTop: 5
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold'
  },
  row: {
    paddingTop: 1
  },
  col: {
    marginRight: 1
  },
  image: {
    width: 192,
    height: 120,
  }
});
