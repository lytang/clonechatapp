import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  ActivityIndicator
} from "react-native";

import {
  Toast
} from "native-base";

import {
  PRIMARY_COLOR
} from '../constants';
import Sound from 'react-native-sound';
// import { NavigationActions } from 'react-navigation';
import { StackActions, NavigationActions } from 'react-navigation';
const configPackage = require('chatApp/package.json');
import {apiUrl} from "../constants/apiUrl";

export default class Login extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props){
    super(props);
    this.state = {
      isSigning: false
    }
  }

  componentWillMount(){
    console.log(this.props.screenProps.userId())
    if (Object.keys(this.props.headers).length !== 0) {
      this.navigateToList();
    }

  }

  navigateToList(){
    // const resetAction = NavigationActions.reset({
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Listing' })],
    });
    // const resetAction = NavigationActions.navigate({
    //   index: 0,
    //   actions: [
    //     NavigationActions.navigate({ routeName: 'Listing' })
    //   ]
    // })
    // this.props.navigation.dispatch(resetAction)
    this.props.navigation.dispatch(resetAction);

    // this.props.navigation.navigate('Listing')
  }

  signedIn(){
    const { phoneNumber } = this.props.login;
    const userId = this.props.screenProps.userId();
      // alert('userId: '+phoneNumber)
    console.log('SignedIn')

    if (phoneNumber != null || phoneNumber != '') {
      this.setState({ isSigning: true })
      console.log(userId)
      fetch(`${apiUrl}/api/login`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          mobile: phoneNumber,
          // deviceId: userId,
        },
      })
      .then((response)=>{

        // alert(JSON.stringify(response))
         console.log('response', response)
        return response.json()
        // alert('success'+json.status)
      })
      .then((json)=>{
        if(json.status == 1){
          // alert(JSON.stringify(this.props))
          console.log('all props',this.props)
          const { updateHeaders } = this.props;
          const { status, msg, token, userId } = json;
          console.log('success', json)
          this.navigateToList();
          this.props.screenProps.setUserToken(token);
          updateHeaders({
            'apitoken': token,
            'currentUserId': userId
          })
          console.log('update header')
        }else{
          // Alert.alert('Wrong Phone Number')
          this.setState({ isSigning: false })
        }
      })
      .catch((error) => {
        // Alert.alert('Error')
        console.error(error);
      });
    } else {
      Alert.alert('Please complete the form correctly')
    }
  }

  renderForm(){
    const { login, updatePhoneNumber } = this.props;
    const { phoneNumber } = login;
    return <View style={styles.formContainer}>
      <TextInput
        value={phoneNumber}
        onChangeText={updatePhoneNumber}
        style={styles.inputBox}
        underlineColorAndroid="rgba(255, 255, 255, 0)"
        placeholder="Enter your phone number"
        placeholderTextColor='rgba(155,154,155,1.0);'
        keyboardType='numeric'
      />
      <TouchableOpacity style={styles.button} onPress={this.signedIn.bind(this)}>
        <Text style={styles.buttonText}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  }
  renderLoading(){
    return (
        <View>
          <Text style={{
            color: PRIMARY_COLOR
          }}>Waiting For Server Response</Text>
          <ActivityIndicator
            color={PRIMARY_COLOR}
            size="large" />
        </View>
      )
  }


  render() {
    return(
      <View style={styles.container}>
        <Image resizeMode='contain' style={styles.logo} source={require('../images/logo.png')} />
        <Text style={styles.appName}> My Chat ( Message ) </Text>
        <Text>Version: {configPackage.version}</Text>
        { this.state.isSigning ? this.renderLoading() : this.renderForm() }

      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    padding: 30,
  },
  appName: {
    fontSize: 24,
    color: PRIMARY_COLOR,
    fontWeight: 'bold',
    marginBottom: 30
  },
  logo: {
    width: 128,
    height: 128,
  },
  formContainer: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: 'stretch'
  },
  inputBox: {
    alignSelf: 'stretch',
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: PRIMARY_COLOR,
    borderRadius: 25,
    height: 42,
    paddingHorizontal: 16,
    marginVertical: 22,
    fontSize: 13,
    color: "rgba(155,154,155,1.0)"
  },
  button: {
    alignSelf: 'stretch',
    backgroundColor: PRIMARY_COLOR,
    borderRadius: 25,
    marginVertical: 0,
    paddingVertical: 10,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "300",
    color: "#fff",
    textAlign: "center"
  }
});
